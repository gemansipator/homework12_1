Домашнее задание №12 (к уроку от 27.07.2023)
ТЕКСТ ЗАДАНИЯ
Создать новый React-проект
Создать 4 компонента:
1й компонент должен быть стилизован с помощью подключения файла SCSS (через import './Component.scss')
2й компонент должен быть стилизован с помощью CSS-модулей (через import styles from './Component.module.scss')
3й компонент должен быть стилизован с помощью библиотеки styled-components.
4й компонент должен включать несколько любых компонентов с разными параметрами из библиотеки компонентов (на выбор Ant Design или MUI).
Стилизация компонентов – на свой вкус. Важно показать понимание способов стилизации и использования библиотеки компонентов